import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import { createBrowserHistory } from "history";

import { MainPage, ListingPage } from "./pages";

const history = createBrowserHistory();

const AppRoute = () => (
    <Router history={history}>
        <Switch>
            <Route exact path="/" component={MainPage} />
            <Route exact path="/listing" component={ListingPage} />
        </Switch>
    </Router>
);

export default AppRoute;
