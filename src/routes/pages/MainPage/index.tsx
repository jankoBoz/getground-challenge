import React from "react";
import { withStyles } from "@material-ui/core/styles";

import { Listing, Footer } from "../../../components";
import { usePopulateListing } from "../../../hooks";

const styles = (theme) => ({
    root: {
        display: "flex",
        "flex-direction": "column",
        "align-items": "center",
        padding: "5vh 0",
    },
});

const Component = (props) => {
    usePopulateListing(1);
    const { classes, theme } = props;
    return (
        <div className={classes.root}>
            <Listing />
            <Footer />
        </div>
    );
};

export const MainPage = withStyles(styles, { withTheme: true })(Component);
