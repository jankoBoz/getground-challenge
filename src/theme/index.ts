import { createMuiTheme } from "@material-ui/core/styles";

export default createMuiTheme({
    palette: {
        primary: {
            main: "#2481d8",
            light: "#d0e6f4",
        },
        text: {
            primary: "#3f3f3f",
            secondary: "#666666",
        }
    },
});
