import { Action as ReduxAction } from "redux";
import { IBook } from "../interfaces";

export interface IAction<T> extends ReduxAction {
    payload?: T;
}

export interface IBookState {
    books: IBook[];
    count: number;
    page: number;
}

export interface IStore {
    book: IBookState;
}
