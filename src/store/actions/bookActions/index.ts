import { Dispatch } from "redux";

import { actionConsts } from "../../../definitions";
import { IBook } from "../../../interfaces";
import { bookService } from "../../../services";
import store from "../../index";

interface IReturn {
    count: number | string;
    books: IBook[] | [];
}

export const bookActions = {
    fetchListing: (page: number = 1) => {
        return async (dispatch: Dispatch) => {
            const res: void | IReturn = await bookService.fetchListing(page);
            if (res && Array.isArray(res.books) && res.books.length) {
                dispatch({
                    payload: {
                        books: res.books,
                    },
                    type: actionConsts.BOOK.LISTING,
                });

                dispatch({
                    payload: {
                        count: res.count,
                    },
                    type: actionConsts.BOOK.LISTING,
                });

                dispatch({
                    payload: {
                        page: page,
                    },
                    type: actionConsts.BOOK.PAGE_NUMBER,
                });
            }
        };
    },
};
