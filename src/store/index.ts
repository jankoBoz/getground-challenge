import { createStore, applyMiddleware, Store } from "redux";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import Reducers from "./reducers";

const store: any = createStore(
    Reducers,
    undefined,
    composeWithDevTools(applyMiddleware(thunkMiddleware))
);

export default store;
