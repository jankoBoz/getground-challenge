import { IBookState, IAction } from "../../IStore";

import { actionConsts } from "../../../definitions";

const INITIAL_STATE: IBookState = {
    books: [],
    count: 0,
    page: 1,
};

type IMapPayload = {};

export const bookReducer = (
    state = INITIAL_STATE,
    action: IAction<IMapPayload>
) => {
    switch (action.type) {
        case actionConsts.BOOK.LISTING:
            return {
                ...state,
                ...action.payload,
            };

        case actionConsts.BOOK.COUNT:
            return {
                ...state,
                ...action.payload,
            };

        case actionConsts.BOOK.PAGE_NUMBER:
            return {
                ...state,
                ...action.payload,
            };

        default:
            return state;
    }
};
