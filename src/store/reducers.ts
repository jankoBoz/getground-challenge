import { combineReducers } from "redux";

import { IStore } from "./IStore";

import { bookReducer } from "./reducers/book";

const rootReducer = combineReducers<IStore>({
    book: bookReducer,
});

export default rootReducer;
