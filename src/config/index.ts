export const config = {
    publicApi:
        process.env.REACT_APP_PUBLIC_API ||
        "http://nyx.vima.ekt.gr:3000/api/books",
    environment: process.env.NODE_ENV,
};
