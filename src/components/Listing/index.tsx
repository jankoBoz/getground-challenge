import React from "react";
import { useSelector } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";

import { BookCard } from "..";
import { IStore } from "../../store/IStore";

const styles = (theme) => ({
    root: {
        width: "94%",
        "max-width": "1200px",
        margin: "20px auto",
    },
    listing: {
        [theme.breakpoints.up("sm")]: {
            display: "flex",
            "flex-wrap": "wrap",
            "flex-direction": "row",
            "justify-content": "space-between",
        },
    },
});

export const Component = (props) => {
    const { books, count } = useSelector((state: IStore) => state.book);
    const { classes, theme } = props;

    return (
        <main className={classes.root}>
            <Typography
                className={classes.title}
                color="textSecondary"
                gutterBottom
            >
                {count} total results
            </Typography>
            <section className={classes.listing}>
                {books ? (
                    books.map((book) => <BookCard book={book} />)
                ) : (
                    <Typography variant="h1" component="h1">
                        Unfortunately {count} books found.
                    </Typography>
                )}
            </section>
        </main>
    );
};
export const Listing = withStyles(styles, { withTheme: true })(Component);
