import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = (theme) => ({
    root: {
        minWidth: 275,
        width: "100%",
        "margin-bottom": "12px",
        [theme.breakpoints.up("sm")]: {
            width: "20%",
        },
    },

    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
});

export const Component = (props) => {
    const { classes, theme, book } = props;

    return (
        <Card className={classes.root} variant="outlined">
            <CardContent>
                <Typography
                    className={classes.title}
                    color="textSecondary"
                    gutterBottom
                >
                    {book.book_author[0]}
                </Typography>
                <Typography variant="h5" component="h2">
                    {book.book_title}
                </Typography>
                <Typography className={classes.pos} color="textSecondary">
                    Year: {book.book_publication_year}
                </Typography>
                <Typography variant="body2" component="p">
                    well meaning and kindly.
                    <br />
                    {'"a benevolent smile"'}
                </Typography>
            </CardContent>
            <CardActions>
                <Typography variant="h5" component="h2">
                    {book.book_pages} pages
                </Typography>
            </CardActions>
        </Card>
    );
};

export const BookCard = withStyles(styles, { withTheme: true })(Component);
