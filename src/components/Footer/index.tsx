import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import Pagination from "@material-ui/lab/Pagination";
import { IStore } from "../../store/IStore";

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            marginTop: theme.spacing(2),
        },
    },
}));

export const Footer = () => {
    const classes = useStyles();
    const history = useHistory();

    const { count, page } = useSelector((state: IStore) => state.book);
    const pushToPage = (_, val) => {
        history.push(`/listing?page=${val}`);
    };
    return (
        <div className={classes.root}>
            <Pagination count={count / 20} page={page} onChange={pushToPage} />
        </div>
    );
};
