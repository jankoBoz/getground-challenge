import { config } from "../../config";

interface IRequestPayload {
    [key: string]: {};
}

const BASE_URL = `${config.publicApi}/api`;

export const http = {
    request: async <A>(
        methodType: string,
        url: string,
        payload?: IRequestPayload
    ): Promise<A> => {
        return new Promise((resolve, reject) => {
            fetch(`${BASE_URL}${url}`, {
                headers: {
                    "content-type": "application/json",
                },
                method: `${methodType}`,
                body: JSON.stringify(payload),
            })
                .then(async (response) => {
                    if (response.status === 200) {
                        return response.json().then(resolve);
                    }
                    return reject(response);
                })
                .catch((e) => {
                    reject(e);
                });
        });
    },
};
