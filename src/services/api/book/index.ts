import { http } from "../..";

import { IBook } from "../../../interfaces";

interface IReturn {
    count: number | string;
    books: IBook[] | [];
}

export const bookService = {
    fetchListing: async (page: number): Promise<void | IReturn> => {
        try {
            const res: IReturn = await http.request("POST", "/books", {
                page,
            });
            if (res && res.books && res.count) {
                return {
                    books: res.books,
                    count: res.count,
                };
            }
        } catch (e) {
            console.log(e);
        }
    },
};
