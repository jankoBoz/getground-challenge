export const actionConsts = {
 BOOK: {
     LISTING: "BOOK_LISTING",
     COUNT: "BOOK_COUNT",
     PAGE_NUMBER: "PAGE_NUMBER"
 }
};
