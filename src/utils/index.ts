export const truncate = (text: string, length: number) => {
    return text.length > length ? `${text.substr(0, length - 1)}...` : text;
};

export const capitalize = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
};

export const delay = (time: number): Promise<unknown> =>
    new Promise((resolve) => setTimeout(resolve, time));

export const aKey = (): string => `${new Date().getTime() * Math.random()}`;
