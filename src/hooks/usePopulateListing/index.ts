import { useEffect } from "react";
import { useDispatch } from "react-redux";

import { bookActions } from "../../store/actions";

export const usePopulateListing = (page: number = 1) => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(bookActions.fetchListing(page));
    }, []);
};
