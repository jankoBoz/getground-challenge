import { useEffect } from "react";
import { useDispatch } from "react-redux";

import { bookActions } from "../../store/actions";
import { useLocation } from "react-router";
import queryString from "query-string";

export const useBookFetch = () => {
    const location = useLocation();
    const dispatch = useDispatch();
    useEffect(() => {
        const { page } = queryString.parse(location.search);
        dispatch(bookActions.fetchListing(parseInt(page as string, 10)));
    }, [location.search]);
};
